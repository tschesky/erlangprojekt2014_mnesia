-module(initBase).
-export([install/1,init_tables/1]).
-export([create_actor/2, create_movie/2, hire_actor/2, find_movie/1, delete_actor/1, delete_movie/1, grant_oscar/2]).

%% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%

-record(actor, {name, actor_oscars}).
                                   
-record(movie, {title, budget, movie_oscars}).
                                   
-record(cast, {title, name}).


install(Nodes) ->
        ok = mnesia:create_schema(Nodes),
        rpc:multicall(Nodes, application, start, [mnesia]),
        init_tables(Nodes).

        
init_tables(Nodes) ->
    mnesia:create_table(actor, [{attributes, record_info(fields, actor)}, {disc_copies, Nodes}]),
                                                
    mnesia:create_table(movie, [{attributes, record_info(fields, movie)}, {disc_copies, Nodes}]),
                                                
    mnesia:create_table(cast, [{attributes, record_info(fields, cast)}, {disc_copies, Nodes}]).


create_actor(Name, Oscars) ->
        F = fun() ->
                mnesia:write(#actor{name = Name,
                                    actor_oscars = Oscars})
        end,
        mnesia:activity(transaction, F).
        
delete_actor(Name) ->
        F = fun() -> mnesia:delete({actor, Name}) end,
        mnesia:activity(transaction, F).
        
create_movie(Title, Budget) ->
        F = fun() ->
                mnesia:write(#movie{ title = Title,
									budget = Budget,
                                    movie_oscars = 0})
        end,
        mnesia:activity(transaction, F).
		
delete_movie(Title) ->
        F = fun() -> mnesia:delete({movie, Title}) end,
        mnesia:activity(transaction, F).
		
		
find_movie(Title) ->
        Pattern = #movie{title = Title, _ = '_'},
        F = fun() ->
                Result = mnesia:match_object(Pattern),
                [{Budget, Movie_oscars} || #movie{budget = Budget, movie_oscars=Movie_oscars} <- Result]
        end,
        mnesia:activity(transaction, F).

        
hire_actor(Title,Name) ->
        F = fun() ->
                case mnesia:read({actor, Name}) =:= [] orelse
                     mnesia:read({movie, Title}) =:= [] of
                true ->
                        {error, unknown_argument};
                false ->
                        mnesia:write(#cast{title = Title,
                                           name = Name})
                end
        end,
        mnesia:activity(transaction,F).        

grant_oscar(Actor_name, Movie_title) ->
	F = fun() ->
		[M] = mnesia:read({movie, Movie_title}),
		[A] = mnesia:read({actor, Actor_name}),
		Ao = A#actor.actor_oscars,
		Mo = M#movie.movie_oscars,
		Ac = A#actor{actor_oscars = Ao + 1},
		Mov = M#movie{movie_oscars = Mo + 1},
		mnesia:write(Ac),
		mnesia:write(Mov)
	end,
	mnesia:transaction(F).

		



